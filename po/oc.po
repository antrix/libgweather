# translation of oc.po to Occitan
# Occitan translation of gnome-applets.
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-applets package.
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay>
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007.
# Cédric Valmary (Tot en òc) <cvalmary@yahoo.fr>, 2015.
# Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2016, 2018.
msgid ""
msgstr ""
"Project-Id-Version: oc\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libgweather/issues\n"
"POT-Creation-Date: 2021-11-23 12:39+0000\n"
"PO-Revision-Date: 2021-12-01 15:56+0100\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Tot En Òc\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0\n"
"X-Launchpad-Export-Date: 2015-05-21 17:56+0000\n"
"X-Project-Style: gnome\n"

#: libgweather/gweather-info.c:108
msgid "variable"
msgstr "variable"

#: libgweather/gweather-info.c:109
msgid "north"
msgstr "nòrd"

#: libgweather/gweather-info.c:109
msgid "north — northeast"
msgstr "nòrd — nòrd-èst"

#: libgweather/gweather-info.c:109
msgid "northeast"
msgstr "nòrd-èst"

#: libgweather/gweather-info.c:109
msgid "east — northeast"
msgstr "èst — sud-èst"

#: libgweather/gweather-info.c:110
msgid "east"
msgstr "nòrd-èst"

#: libgweather/gweather-info.c:110
msgid "east — southeast"
msgstr "èst — sud-èst"

#: libgweather/gweather-info.c:110
msgid "southeast"
msgstr "sud-èst"

#: libgweather/gweather-info.c:110
msgid "south — southeast"
msgstr "sud — sud-èst"

#: libgweather/gweather-info.c:111
msgid "south"
msgstr "sud"

#: libgweather/gweather-info.c:111
msgid "south — southwest"
msgstr "sud — sud-oèst"

#: libgweather/gweather-info.c:111
msgid "southwest"
msgstr "sud-oèst"

#: libgweather/gweather-info.c:111
msgid "west — southwest"
msgstr "oèst — sud-oèst"

#: libgweather/gweather-info.c:112
msgid "west"
msgstr "oèst"

#: libgweather/gweather-info.c:112
msgid "west — northwest"
msgstr "oèst — nòrd-oèst"

#: libgweather/gweather-info.c:112
msgid "northwest"
msgstr "nòrd-oèst"

#: libgweather/gweather-info.c:112
msgid "north — northwest"
msgstr "nòrd — nòrd-oèst"

#: libgweather/gweather-info.c:116
msgid "Variable"
msgstr "Variable"

#: libgweather/gweather-info.c:117
msgid "North"
msgstr "Nòrd"

#: libgweather/gweather-info.c:117
msgid "North — Northeast"
msgstr "Nòrd — Nord-Èst"

#: libgweather/gweather-info.c:117
msgid "Northeast"
msgstr "Nòrd-Èst"

#: libgweather/gweather-info.c:117
msgid "East — Northeast"
msgstr "Èst — Nord-Èst"

#: libgweather/gweather-info.c:118
msgid "East"
msgstr "Èst"

#: libgweather/gweather-info.c:118
msgid "East — Southeast"
msgstr "Èst — Sud-Èst"

#: libgweather/gweather-info.c:118
msgid "Southeast"
msgstr "Sud-Èst"

#: libgweather/gweather-info.c:118
msgid "South — Southeast"
msgstr "Sud — Sud-Èst"

#: libgweather/gweather-info.c:119
msgid "South"
msgstr "Sud"

#: libgweather/gweather-info.c:119
msgid "South — Southwest"
msgstr "Sud — Sud-Oèst"

#: libgweather/gweather-info.c:119
msgid "Southwest"
msgstr "Sud-Oèst"

#: libgweather/gweather-info.c:119
msgid "West — Southwest"
msgstr "Oèst — Sud-Oèst"

#: libgweather/gweather-info.c:120
msgid "West"
msgstr "Oèst"

#: libgweather/gweather-info.c:120
msgid "West — Northwest"
msgstr "Oèst — Nord-Oèst"

#: libgweather/gweather-info.c:120
msgid "Northwest"
msgstr "Nòrd-Oèst"

#: libgweather/gweather-info.c:120
msgid "North — Northwest"
msgstr "Nòrd — Nòrd-Oèst"

#: libgweather/gweather-info.c:131
msgctxt "wind direction"
msgid "Invalid"
msgstr "Invalid"

#: libgweather/gweather-info.c:132
msgctxt "wind direction"
msgid "invalid"
msgstr "invalid"

#: libgweather/gweather-info.c:145
msgid "clear sky"
msgstr "cèl seren"

#: libgweather/gweather-info.c:146
msgid "broken clouds"
msgstr "fòrça pauc de nívols"

#: libgweather/gweather-info.c:147
msgid "scattered clouds"
msgstr "nívols escampilhadas"

#: libgweather/gweather-info.c:148
msgid "few clouds"
msgstr "qualques nívols"

#: libgweather/gweather-info.c:149
msgid "overcast"
msgstr "cobèrt"

#: libgweather/gweather-info.c:153
msgid "Clear sky"
msgstr "Cèl seren"

#: libgweather/gweather-info.c:154
msgid "Broken clouds"
msgstr "Nívols escaras"

#: libgweather/gweather-info.c:155
msgid "Scattered clouds"
msgstr "Nívols escampilhadas"

#: libgweather/gweather-info.c:156
msgid "Few clouds"
msgstr "Qualques nívols"

#: libgweather/gweather-info.c:157
msgid "Overcast"
msgstr "Ennivolat"

#: libgweather/gweather-info.c:173 libgweather/gweather-info.c:290
msgctxt "sky conditions"
msgid "Invalid"
msgstr "Invalidas"

#: libgweather/gweather-info.c:174 libgweather/gweather-info.c:291
msgctxt "sky conditions"
msgid "invalid"
msgstr "invalid"

#. TRANSLATOR: If you want to know what "blowing" "shallow" "partial"
#. * etc means, you can go to http://www.weather.com/glossary/ and
#. * http://www.crh.noaa.gov/arx/wx.tbl.php
#. NONE
#: libgweather/gweather-info.c:208 libgweather/gweather-info.c:210
msgid "thunderstorm"
msgstr "auratge"

#. DRIZZLE
#: libgweather/gweather-info.c:209
msgid "drizzle"
msgstr "plovina"

#: libgweather/gweather-info.c:209
msgid "light drizzle"
msgstr "plovina leugièra"

#: libgweather/gweather-info.c:209
msgid "moderate drizzle"
msgstr "plovina moderada"

#: libgweather/gweather-info.c:209
msgid "heavy drizzle"
msgstr "plovina pesuga"

#: libgweather/gweather-info.c:209
msgid "freezing drizzle"
msgstr "plovina gelibranta"

#. RAIN
#: libgweather/gweather-info.c:210
msgid "rain"
msgstr "pluèja"

#: libgweather/gweather-info.c:210
msgid "light rain"
msgstr "pluèja leugièra"

#: libgweather/gweather-info.c:210
msgid "moderate rain"
msgstr "pluèja moderada"

#: libgweather/gweather-info.c:210
msgid "heavy rain"
msgstr "pluèja druda"

#: libgweather/gweather-info.c:210
msgid "rain showers"
msgstr "raissas"

#: libgweather/gweather-info.c:210
msgid "freezing rain"
msgstr "pluèja gelibranta"

#. SNOW
#: libgweather/gweather-info.c:211
msgid "snow"
msgstr "nèu"

#: libgweather/gweather-info.c:211
msgid "light snow"
msgstr "nèu leugièra"

#: libgweather/gweather-info.c:211
msgid "moderate snow"
msgstr "nèu moderada"

#: libgweather/gweather-info.c:211
msgid "heavy snow"
msgstr "nèu violenta"

#: libgweather/gweather-info.c:211
msgid "snowstorm"
msgstr "tempèsta de nèu"

#: libgweather/gweather-info.c:211
msgid "blowing snowfall"
msgstr "nevada e vent"

#: libgweather/gweather-info.c:211
msgid "snow showers"
msgstr "precipitacions de nèu"

#: libgweather/gweather-info.c:211
msgid "drifting snow"
msgstr "amontetament de nèu"

#. SNOW_GRAINS
#: libgweather/gweather-info.c:212
msgid "snow grains"
msgstr "borrilhs de nèu"

#: libgweather/gweather-info.c:212
msgid "light snow grains"
msgstr "leugièrs borrilhs de nèu"

#: libgweather/gweather-info.c:212
msgid "moderate snow grains"
msgstr "borrilhs de nèu moderats"

#: libgweather/gweather-info.c:212
msgid "heavy snow grains"
msgstr "borrilhs de nèu violents"

#. ICE_CRYSTALS
#: libgweather/gweather-info.c:213
msgid "ice crystals"
msgstr "polverèls de glaç"

#. ICE_PELLETS
#: libgweather/gweather-info.c:214
msgid "sleet"
msgstr "granissa"

#: libgweather/gweather-info.c:214
msgid "little sleet"
msgstr "granissa leugièra"

#: libgweather/gweather-info.c:214
msgid "moderate sleet"
msgstr "granissa moderada"

#: libgweather/gweather-info.c:214
msgid "heavy sleet"
msgstr "granissa violenta"

#: libgweather/gweather-info.c:214
msgid "sleet storm"
msgstr "tempèsta de granissa"

#: libgweather/gweather-info.c:214
msgid "showers of sleet"
msgstr "raissas de granissa"

#. HAIL
#: libgweather/gweather-info.c:215
msgid "hail"
msgstr "grèla"

#: libgweather/gweather-info.c:215
msgid "hailstorm"
msgstr "tempèsta de pèiras"

#: libgweather/gweather-info.c:215
msgid "hail showers"
msgstr "raissas de pèiras"

#. SMALL_HAIL
#: libgweather/gweather-info.c:216
msgid "small hail"
msgstr "pichona grèla"

#: libgweather/gweather-info.c:216
msgid "small hailstorm"
msgstr "tempèsta de pichona grèla"

#: libgweather/gweather-info.c:216
msgid "showers of small hail"
msgstr "raissas de pichona grèla"

#. PRECIPITATION
#: libgweather/gweather-info.c:217
msgid "unknown precipitation"
msgstr "precipitacions desconegudas"

#. MIST
#: libgweather/gweather-info.c:218
msgid "mist"
msgstr "bruma"

#. FOG
#: libgweather/gweather-info.c:219
msgid "fog"
msgstr "nèbla"

#: libgweather/gweather-info.c:219
msgid "fog in the vicinity"
msgstr "nèbla empr'aquí"

#: libgweather/gweather-info.c:219
msgid "shallow fog"
msgstr "nèbla superficial"

#: libgweather/gweather-info.c:219
msgid "patches of fog"
msgstr "capas de nèbla"

#: libgweather/gweather-info.c:219
msgid "partial fog"
msgstr "nèbla intermitenta"

#: libgweather/gweather-info.c:219
msgid "freezing fog"
msgstr "nèbla gelibranta"

#. SMOKE
#: libgweather/gweather-info.c:220
msgid "smoke"
msgstr "fum"

#. VOLCANIC_ASH
#: libgweather/gweather-info.c:221
msgid "volcanic ash"
msgstr "cendres volcanicas"

#. SAND
#: libgweather/gweather-info.c:222
msgid "sand"
msgstr "sabla"

#: libgweather/gweather-info.c:222
msgid "blowing sand"
msgstr "sabla ventosa"

#: libgweather/gweather-info.c:222
msgid "drifting sand"
msgstr "amontetament de sabla"

#. HAZE
#: libgweather/gweather-info.c:223
msgid "haze"
msgstr "bruma"

#. SPRAY
#: libgweather/gweather-info.c:224
msgid "blowing sprays"
msgstr "embruns e vent"

#. DUST
#: libgweather/gweather-info.c:225
msgid "dust"
msgstr "posca"

#: libgweather/gweather-info.c:225
msgid "blowing dust"
msgstr "posca e vent"

#: libgweather/gweather-info.c:225
msgid "drifting dust"
msgstr "amontetament de posca"

#. SQUALL
#: libgweather/gweather-info.c:226
msgid "squall"
msgstr "ventòrla"

#. SANDSTORM
#: libgweather/gweather-info.c:227
msgid "sandstorm"
msgstr "tempèsta de sabla"

#: libgweather/gweather-info.c:227
msgid "sandstorm in the vicinity"
msgstr "tempèsta de sabla empr'aquí"

#: libgweather/gweather-info.c:227
msgid "heavy sandstorm"
msgstr "tempèsta de sabla violenta"

#. DUSTSTORM
#: libgweather/gweather-info.c:228
msgid "duststorm"
msgstr "tempèsta de posca"

#: libgweather/gweather-info.c:228
msgid "duststorm in the vicinity"
msgstr "tempèsta de posca empr'aquí"

#: libgweather/gweather-info.c:228
msgid "heavy duststorm"
msgstr "tempèsta de posca violenta"

#. FUNNEL_CLOUD
#: libgweather/gweather-info.c:229
msgid "funnel cloud"
msgstr "mini tornada"

#. TORNADO
#: libgweather/gweather-info.c:230
msgid "tornado"
msgstr "tornada"

#. DUST_WHIRLS
#: libgweather/gweather-info.c:231
msgid "dust whirls"
msgstr "revolums de posca"

#: libgweather/gweather-info.c:231
msgid "dust whirls in the vicinity"
msgstr "revolums de posca empr'aquí"

#. TRANSLATOR: If you want to know what "blowing" "shallow" "partial"
#. * etc means, you can go to http://www.weather.com/glossary/ and
#. * http://www.crh.noaa.gov/arx/wx.tbl.php
#. NONE
#: libgweather/gweather-info.c:244 libgweather/gweather-info.c:246
msgid "Thunderstorm"
msgstr "Auratge"

#. DRIZZLE
#: libgweather/gweather-info.c:245
msgid "Drizzle"
msgstr "Plovina"

#: libgweather/gweather-info.c:245
msgid "Light drizzle"
msgstr "Plovina leugièra"

#: libgweather/gweather-info.c:245
msgid "Moderate drizzle"
msgstr "Plovina moderada"

#: libgweather/gweather-info.c:245
msgid "Heavy drizzle"
msgstr "Gròssa ramada"

#: libgweather/gweather-info.c:245
msgid "Freezing drizzle"
msgstr "Plovina gelibranta"

#. RAIN
#: libgweather/gweather-info.c:246
msgid "Rain"
msgstr "Pluèja"

#: libgweather/gweather-info.c:246
msgid "Light rain"
msgstr "Plovina"

#: libgweather/gweather-info.c:246
msgid "Moderate rain"
msgstr "Pluèja moderada"

#: libgweather/gweather-info.c:246
msgid "Heavy rain"
msgstr "Fòrta pluèja"

#: libgweather/gweather-info.c:246
msgid "Rain showers"
msgstr "Raissas"

#: libgweather/gweather-info.c:246
msgid "Freezing rain"
msgstr "Pluèja gelibranta"

#. SNOW
#: libgweather/gweather-info.c:247
msgid "Snow"
msgstr "Nèu"

#: libgweather/gweather-info.c:247
msgid "Light snow"
msgstr "Nèu leugièra"

#: libgweather/gweather-info.c:247
msgid "Moderate snow"
msgstr "Nèu moderada"

#: libgweather/gweather-info.c:247
msgid "Heavy snow"
msgstr "Fòrça nèu"

#: libgweather/gweather-info.c:247
msgid "Snowstorm"
msgstr "Tempèsta de nèu"

#: libgweather/gweather-info.c:247
msgid "Blowing snowfall"
msgstr "Nèu e vent"

#: libgweather/gweather-info.c:247
msgid "Snow showers"
msgstr "Nevadas"

#: libgweather/gweather-info.c:247
msgid "Drifting snow"
msgstr "Amontetament de nèu"

#. SNOW_GRAINS
#: libgweather/gweather-info.c:248
msgid "Snow grains"
msgstr "Borrilhs"

#: libgweather/gweather-info.c:248
msgid "Light snow grains"
msgstr "Borrilhs de nèu pichons"

#: libgweather/gweather-info.c:248
msgid "Moderate snow grains"
msgstr "Borrilhs moderats"

#: libgweather/gweather-info.c:248
msgid "Heavy snow grains"
msgstr "Borrilhs de nèu bèls"

#. ICE_CRYSTALS
#: libgweather/gweather-info.c:249
msgid "Ice crystals"
msgstr "Cristals de glaç"

#. ICE_PELLETS
#: libgweather/gweather-info.c:250
msgid "Sleet"
msgstr "Nèu fonduda"

#: libgweather/gweather-info.c:250
msgid "Little sleet"
msgstr "Nèu fonduda leugièra"

#: libgweather/gweather-info.c:250
msgid "Moderate sleet"
msgstr "Nèu fonduda moderada"

#: libgweather/gweather-info.c:250
msgid "Heavy sleet"
msgstr "Nèu fonduda violenta"

#: libgweather/gweather-info.c:250
msgid "Sleet storm"
msgstr "Tempèsta de nèu fonduda"

#: libgweather/gweather-info.c:250
msgid "Showers of sleet"
msgstr "Raissas de nèu fonduda"

#. HAIL
#: libgweather/gweather-info.c:251
msgid "Hail"
msgstr "Granissa"

#: libgweather/gweather-info.c:251
msgid "Hailstorm"
msgstr "Tempèsta de pèiras"

#: libgweather/gweather-info.c:251
msgid "Hail showers"
msgstr "Granissadas"

#. SMALL_HAIL
#: libgweather/gweather-info.c:252
msgid "Small hail"
msgstr "Pichonas pèiras"

#: libgweather/gweather-info.c:252
msgid "Small hailstorm"
msgstr "Pichona tempèsta de pèiras"

#: libgweather/gweather-info.c:252
msgid "Showers of small hail"
msgstr "Raissas de pichonas pèiras"

#. PRECIPITATION
#: libgweather/gweather-info.c:253
msgid "Unknown precipitation"
msgstr "Precipitacions desconegudas"

#. MIST
#: libgweather/gweather-info.c:254
msgid "Mist"
msgstr "Nèbla"

#. FOG
#: libgweather/gweather-info.c:255
msgid "Fog"
msgstr "Fums"

#: libgweather/gweather-info.c:255
msgid "Fog in the vicinity"
msgstr "Fums empr'aquí"

#: libgweather/gweather-info.c:255
msgid "Shallow fog"
msgstr "Fums superficial"

#: libgweather/gweather-info.c:255
msgid "Patches of fog"
msgstr "Capas de nèbla"

#: libgweather/gweather-info.c:255
msgid "Partial fog"
msgstr "Nèbla irregulara"

#: libgweather/gweather-info.c:255
msgid "Freezing fog"
msgstr "Nèbla gelibranta"

#. SMOKE
#: libgweather/gweather-info.c:256
msgid "Smoke"
msgstr "Fum"

#. VOLCANIC_ASH
#: libgweather/gweather-info.c:257
msgid "Volcanic ash"
msgstr "Cendres volcanics"

#. SAND
#: libgweather/gweather-info.c:258
msgid "Sand"
msgstr "Sabla"

#: libgweather/gweather-info.c:258
msgid "Blowing sand"
msgstr "Vent sablós"

#: libgweather/gweather-info.c:258
msgid "Drifting sand"
msgstr "Amontetament de sabla"

#. HAZE
#: libgweather/gweather-info.c:259
msgid "Haze"
msgstr "Bruma leugièra"

#. SPRAY
#: libgweather/gweather-info.c:260
msgid "Blowing sprays"
msgstr "Esposques e vent"

#. DUST
#: libgweather/gweather-info.c:261
msgid "Dust"
msgstr "Vents de sabla"

#: libgweather/gweather-info.c:261
msgid "Blowing dust"
msgstr "Posca e vent"

#: libgweather/gweather-info.c:261
msgid "Drifting dust"
msgstr "Amontetament de posca"

#. SQUALL
#: libgweather/gweather-info.c:262
msgid "Squall"
msgstr "Bofaniá"

#. SANDSTORM
#: libgweather/gweather-info.c:263
msgid "Sandstorm"
msgstr "Tempèsta de sabla"

#: libgweather/gweather-info.c:263
msgid "Sandstorm in the vicinity"
msgstr "Tempèsta de sabla empr'aquí"

#: libgweather/gweather-info.c:263
msgid "Heavy sandstorm"
msgstr "Tempèsta de sabla violenta"

#. DUSTSTORM
#: libgweather/gweather-info.c:264
msgid "Duststorm"
msgstr "Tempèsta de posca"

#: libgweather/gweather-info.c:264
msgid "Duststorm in the vicinity"
msgstr "Tempèsta de posca empr'aquí"

#: libgweather/gweather-info.c:264
msgid "Heavy duststorm"
msgstr "Tempèsta de posca violenta"

#. FUNNEL_CLOUD
#: libgweather/gweather-info.c:265
msgid "Funnel cloud"
msgstr "Tromba"

#. TORNADO
#: libgweather/gweather-info.c:266
msgid "Tornado"
msgstr "Tornada"

#. DUST_WHIRLS
#: libgweather/gweather-info.c:267
msgid "Dust whirls"
msgstr "Revolums de posca"

#: libgweather/gweather-info.c:267
msgid "Dust whirls in the vicinity"
msgstr "Revolums de posca empr'aquí"

#: libgweather/gweather-info.c:825
msgid "%a, %b %d / %H∶%M"
msgstr "%a %d %b / %H∶%M"

#: libgweather/gweather-info.c:831
msgid "Unknown observation time"
msgstr "Data d'observacion desconeguda"

#: libgweather/gweather-info.c:843
msgctxt "sky conditions"
msgid "Unknown"
msgstr "Desconegudas"

#. Translate to the default units to use for presenting
#. * lengths to the user. Translate to default:inch if you
#. * want inches, otherwise translate to default:mm.
#. * Do *not* translate it to "predefinito:mm", if it
#. * it isn't default:mm or default:inch it will not work
#.
#: libgweather/gweather-info.c:874
msgid "default:mm"
msgstr "default:mm"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit
#. * with a non-break space (U+00A0) between the digits and the
#. * degrees sign (U+00B0), followed by the letter F
#.
#: libgweather/gweather-info.c:921
#, c-format
msgid "%.1f °F"
msgstr "%.1f °F"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit
#. * with a non-break space (U+00A0) between the digits and the
#. * degrees sign (U+00B0), followed by the letter F
#.
#: libgweather/gweather-info.c:927
#, c-format
msgid "%d °F"
msgstr "%d °F"

#. TRANSLATOR: This is the temperature in degrees Celsius
#. * with a non-break space (U+00A0) between the digits and
#. * the degrees sign (U+00B0), followed by the letter C
#.
#: libgweather/gweather-info.c:936
#, c-format
msgid "%.1f °C"
msgstr "%.1f °C"

#. TRANSLATOR: This is the temperature in degrees Celsius
#. * with a non-break space (U+00A0) between the digits and
#. * the degrees sign (U+00B0), followed by the letter C
#.
#: libgweather/gweather-info.c:942
#, c-format
msgid "%d °C"
msgstr "%d °C"

#. TRANSLATOR: This is the temperature in kelvin (U+212A KELVIN SIGN)
#. * with a non-break space (U+00A0) between the digits and the degrees
#. * sign
#.
#: libgweather/gweather-info.c:951
#, c-format
msgid "%.1f K"
msgstr "%.1f K"

#. TRANSLATOR: This is the temperature in kelvin (U+212A KELVIN SIGN)
#. * with a non-break space (U+00A0) between the digits and the degrees
#. * sign
#.
#: libgweather/gweather-info.c:957
#, c-format
msgid "%d K"
msgstr "%d K"

#: libgweather/gweather-info.c:977 libgweather/gweather-info.c:990
#: libgweather/gweather-info.c:1003 libgweather/gweather-info.c:1061
msgctxt "temperature"
msgid "Unknown"
msgstr "Desconeguda"

#: libgweather/gweather-info.c:1023
msgctxt "dew"
msgid "Unknown"
msgstr "Desconeguda"

#: libgweather/gweather-info.c:1043
msgctxt "humidity"
msgid "Unknown"
msgstr "Desconeguda"

#. TRANSLATOR: This is the humidity in percent
#: libgweather/gweather-info.c:1046
#, c-format
msgid "%.f%%"
msgstr "%.f %%"

#. TRANSLATOR: This is the wind speed in knots
#: libgweather/gweather-info.c:1088
#, c-format
msgid "%0.1f knots"
msgstr "%0.1f nosèls"

#. TRANSLATOR: This is the wind speed in miles per hour
#: libgweather/gweather-info.c:1091
#, c-format
msgid "%.1f mph"
msgstr "%.1f mph"

#. TRANSLATOR: This is the wind speed in kilometers per hour
#: libgweather/gweather-info.c:1094
#, c-format
msgid "%.1f km/h"
msgstr "%.1f km/h"

#. TRANSLATOR: This is the wind speed in meters per second
#: libgweather/gweather-info.c:1097
#, c-format
msgid "%.1f m/s"
msgstr "%.1f m/s"

#. TRANSLATOR: This is the wind speed as a Beaufort force factor
#. * (commonly used in nautical wind estimation).
#.
#: libgweather/gweather-info.c:1102
#, c-format
msgid "Beaufort force %.1f"
msgstr "Fòrça %.1f Beaufort"

#: libgweather/gweather-info.c:1119
msgctxt "wind speed"
msgid "Unknown"
msgstr "Desconeguda"

#: libgweather/gweather-info.c:1121
msgid "Calm"
msgstr "Calm"

#. TRANSLATOR: This is 'wind direction' / 'wind speed'
#: libgweather/gweather-info.c:1129
#, c-format
msgid "%s / %s"
msgstr "%s / %s"

#. TRANSLATOR: This is the wind speed in knots
#: libgweather/gweather-info.c:1150
msgid "knots"
msgstr "nosèls"

#. TRANSLATOR: This is the wind speed in miles per hour
#: libgweather/gweather-info.c:1153
msgid "mph"
msgstr "mph"

#. TRANSLATOR: This is the wind speed in kilometers per hour
#: libgweather/gweather-info.c:1156
msgid "km/h"
msgstr "km/h"

#. TRANSLATOR: This is the wind speed in meters per second
#: libgweather/gweather-info.c:1159
msgid "m/s"
msgstr "m/s"

#. TRANSLATOR: This is the wind speed as a Beaufort force factor
#. * (commonly used in nautical wind estimation).
#.
#: libgweather/gweather-info.c:1164
msgid "Beaufort force"
msgstr "Fòrça Beaufort"

#: libgweather/gweather-info.c:1197
msgctxt "pressure"
msgid "Unknown"
msgstr "Desconeguda"

#. TRANSLATOR: This is pressure in inches of mercury
#: libgweather/gweather-info.c:1203
#, c-format
msgid "%.2f inHg"
msgstr "%.2f inHg"

#. TRANSLATOR: This is pressure in millimeters of mercury
#: libgweather/gweather-info.c:1206
#, c-format
msgid "%.1f mmHg"
msgstr "%.1f mmHg"

#. TRANSLATOR: This is pressure in kiloPascals
#: libgweather/gweather-info.c:1209
#, c-format
msgid "%.2f kPa"
msgstr "%.2f kPa"

#. TRANSLATOR: This is pressure in hectoPascals
#: libgweather/gweather-info.c:1212
#, c-format
msgid "%.2f hPa"
msgstr "%.2f hPa"

#. TRANSLATOR: This is pressure in millibars
#: libgweather/gweather-info.c:1215
#, c-format
msgid "%.2f mb"
msgstr "%.2f mb"

#. TRANSLATOR: This is pressure in atmospheres
#: libgweather/gweather-info.c:1218
#, c-format
msgid "%.3f atm"
msgstr "%.3f atm"

#: libgweather/gweather-info.c:1252
msgctxt "visibility"
msgid "Unknown"
msgstr "Desconeguda"

#. TRANSLATOR: This is the visibility in miles
#: libgweather/gweather-info.c:1258
#, c-format
msgid "%.1f miles"
msgstr "%.1f milas anglesas"

#. TRANSLATOR: This is the visibility in kilometers
#: libgweather/gweather-info.c:1261
#, c-format
msgid "%.1f km"
msgstr "%.1f km"

#. TRANSLATOR: This is the visibility in meters
#: libgweather/gweather-info.c:1264
#, c-format
msgid "%.0fm"
msgstr "%.0fm"

#: libgweather/gweather-info.c:1289 libgweather/gweather-info.c:1311
msgid "%H∶%M"
msgstr "%H∶%M"

#: libgweather/gweather-info.c:1386
msgid "Retrieval failed"
msgstr "La recuperacion a fracassat"

#: libgweather/weather-owm.c:384
msgid ""
"Weather data from the <a href=\"https://openweathermap.org\">Open Weather "
"Map project</a>"
msgstr ""
"Donadas meteorologicas del <a href=\"http://openweathermap.org\">projècte "
"Open Weather Map </a>"

#. The new (documented but not advertised) API is less strict in the
#. format of the attribution, and just requires a generic CC-BY compatible
#. attribution with a link to their service.
#.
#. That's very nice of them!
#.
#: libgweather/weather-metno.c:374
msgid ""
"Weather data from the <a href=\"https://www.met.no/\">Norwegian "
"Meteorological Institute</a>."
msgstr ""
"Donadas meteorologicas de <a href=\"http://www.met.no/\">l'institut "
"meteorologic norvegian</a>."

#: schemas/org.gnome.GWeather4.gschema.xml:5
msgid "Temperature unit"
msgstr "Unitat de temperatura"

#: schemas/org.gnome.GWeather4.gschema.xml:6
msgid ""
"The unit of temperature used for showing weather. Valid values are “kelvin”, "
"“centigrade” and “fahrenheit”."
msgstr ""
"L'unitat de temperatura utilizada per afichar la metèo. Las valors validas "
"son « kelvin », « centigrade » e « fahrenheit »."

#: schemas/org.gnome.GWeather4.gschema.xml:13
msgid "Distance unit"
msgstr "Unitat de distància"

#: schemas/org.gnome.GWeather4.gschema.xml:14
msgid ""
"The unit of distance used for showing weather (for example for visibility or "
"for distance of important events). Valid values are “meters”, “km” and "
"“miles”."
msgstr ""
"L'unitat de distància utilizada per afichar la metèo (per exemple per la "
"visibilitat o per la distància d'eveniments importants). Las valors validas "
"son « meters », « km » e « miles »."

#: schemas/org.gnome.GWeather4.gschema.xml:21
msgid "Speed unit"
msgstr "Unitat de velocitat"

#: schemas/org.gnome.GWeather4.gschema.xml:22
msgid ""
"The unit of speed used for showing weather (for example for wind speed). "
"Valid values are “ms” (meters per second), “kph” (kilometers per hour), "
"“mph” (miles per hour), “knots” and “bft” (Beaufort scale)."
msgstr ""
"L'unitat de velocitat utilizada per afichar la metèo (per exemple per la "
"velocitat del vent). Las valors validas son « ms » (mètres per segonda), « "
"kph » (quilomètres per ora), « mph » (miles per ora), « knots » (nosèls) e « "
"bft » (escala de Beaufort)."

#: schemas/org.gnome.GWeather4.gschema.xml:30
msgid "Pressure unit"
msgstr "Unitat de pression"

#: schemas/org.gnome.GWeather4.gschema.xml:31
msgid ""
"The unit of pressure used for showing weather. Valid values are "
"“kpa” (kilopascal), “hpa” (hectopascal), “mb” (millibar, mathematically "
"equivalent to 1 hPa but shown differently), “mm-hg” (millimeters of "
"mercury), “inch-hg” (inches of mercury), “atm” (atmospheres)."
msgstr ""
"L'unitat de pression utilizada per afichar la metèo. Las valors validas son "
"« kpa » (quilopascal), « hpa » (ectopascal), « mb » (millibar, "
"matematicament equivalent a 1 hPa mas afichat diferentament), « mm-hg "
"» (millimètre de mercuri), « po-hg » (poces de mercuri), « atm "
"» (atmosfèras)."

#. TRANSLATORS: pick a default location to use in the weather applet. This should
#. usually be the largest city or the capital of your country. If you're not picking
#. a <location> in the database, don't forget to set name and coordinates.
#. Do NOT change or localize the quotation marks!
#: schemas/org.gnome.GWeather4.gschema.xml:43
msgid "('', 'KNYC', nothing)"
msgstr "('', 'LFBO', nothing)"

#: schemas/org.gnome.GWeather4.gschema.xml:44
msgid "Default location"
msgstr "Emplaçament per defaut"

#: schemas/org.gnome.GWeather4.gschema.xml:45
msgid ""
"The default location for the weather applet. The first field is the name "
"that will be shown. If empty, it will be taken from the locations database. "
"The second field is the METAR code for the default weather station. It must "
"not be empty and must correspond to a <code> tag in the Locations.xml file. "
"The third field is a tuple of (latitude, longitude), to override the value "
"taken from the database. This is only used for sunrise and moon phase "
"calculations, not for weather forecast."
msgstr ""
"L'emplaçament per defaut per l'applet de la metèo. Lo primièr camp es lo nom "
"que serà afichat. Se void, serà pres dins la basa de donadas dels sites. Lo "
"segond camp es lo còdi METAR per l'estacion metèo per defaut. Deu pas èsser "
"void e deu correspondre a una balisa &lt;còdi&gt; contengut dins lo fichièr "
"Locations.xml. Lo tresen camp es una para (latitud, longitud) que pren la "
"plaça de la valor presa dins la basa de donadas. Aqueste darrièr es pas "
"utilizat que pels calculs de levada del solelh e fasa de la luna, pas per "
"las previsions meteorologicas."

#~ msgid "Greenwich Mean Time"
#~ msgstr "Temps mejan de Greenwich"

#~ msgid "Failed to get METAR data: %d %s.\n"
#~ msgstr "Impossible d'obténer las donadas METAR : %d %s.\n"

#~ msgid "%s, %s"
#~ msgstr "%s, %s"

#~ msgid "Loading…"
#~ msgstr "Cargament…"

# Se rapporte à "Fuseau horaire"
#~ msgctxt "timezone"
#~ msgid "Unknown"
#~ msgstr "Desconegut"

#~ msgid "%.1f ℉"
#~ msgstr "%.1f ℉"

#~ msgid "%d ℉"
#~ msgstr "%d ℉"

#~ msgid "%.1f ℃"
#~ msgstr "%.1f ℃"

#~ msgid "%d ℃"
#~ msgstr "%d ℃"

#~ msgid "URL for the radar map"
#~ msgstr "URL de la mapa radar"

#~ msgid ""
#~ "The custom URL from where to retrieve a radar map, or empty for disabling "
#~ "radar maps."
#~ msgstr ""
#~ "L'URL personalizat dempuèi lo qual cal recuperar una mapa radar o voida "
#~ "per desactivar las mapas radar."

#~| msgctxt "sky conditions"
#~| msgid "Invalid"
#~ msgctxt "sky conditions"
#~ msgid "invalidas"
#~ msgstr "invalidas"

#~ msgid "%d ℉"
#~ msgstr "%d ℉"

#~ msgid "%d ℃"
#~ msgstr "%d ℃"

#~ msgid "%d K"
#~ msgstr "%d K"

#~ msgid "Location Entry"
#~ msgstr "Zòna de picada de l'emplaçament"

#~ msgid "Timezone Menu"
#~ msgstr "Menú del fus orari"

#~ msgid "Timezone"
#~ msgstr "Fus orari"

#~ msgid "GWeather"
#~ msgstr "GWeather"

#~ msgid "%d °F"
#~ msgstr "%d °F"

#~ msgid "%d °C"
#~ msgstr "%d °C"

#~ msgid "%.1f K"
#~ msgstr "%.1f K"

#~ msgid "%d K"
#~ msgstr "%d K"

#~ msgid "%d ےF"
#~ msgstr "%d ےF"

#~ msgid "%d ےC"
#~ msgstr "%d ےC"
